// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HomeWork20GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORK20_API AHomeWork20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
